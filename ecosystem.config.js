module.exports = {
	apps: [{
		name: 'kalyani frontend',
		script: 'http-server',
		env: {
			NODE_ENV: 'development'
		},
		env_production: {
			NODE_ENV: 'production'
		}
	}],
	deploy: {
		production: {
			'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
		}
	}
}