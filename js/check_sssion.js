(async () => {
    const token = localStorage.getItem('Authorization');
    if (token ===  null || token  === '' ||  token === undefined) {
        window.parent.location = "./login.html";
    } else {
        try {
            const username = localStorage.getItem('userName');
            const request = new Request('http://15.206.64.54:4000/verifySession', {
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'authorization': token
                }),
                body: JSON.stringify({
                    username
                }) 
            });
            const fetchReponse = await fetch(request);
            const response = await fetchReponse.json();
           if(response.error) {
            window.parent.location = "./login.html";
           }
        } catch (error) {
            window.parent.location = "./login.html";
        }
    }
})();